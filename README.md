# Práctica 1 - Construir un ambiente de laboratorio

# Elaborado por: 
    Carlos Cotera
    Daniel Muñoz

## Evidencias de instalación y configuración de Khali Linux

1. Nombre de host

    ![Host Kali](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/KhaliHost.jpeg)
    
    ![Ip Kali](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/IpKali.jpeg)
    
2. Actualización del sistema operativo

    ![Update Kali](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/UpgradeKhali.jpeg)
    
    ![Upgrade Kali](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/UpgradeKhali2.jpeg)
    
3. Instalación de Virtual Box Guest Aditions

    ![Guest Kali](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/VirtualBoxGuest.jpeg)
    
    ![Drag Kali](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/Drag.jpeg)
    
    ![Drop Kali](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/Drop.jpeg)
    
4. Conectividad entre Kali y la tarjeta ROJA de Smoothwall

    ![Conectividad Kali](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/Roja.jpeg)
    
## Evidencias de Smoothwall

1. Imagenes de evidencia de Smoothwall

    ![Smoothwall](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/Smoothwall.jpeg)
    
    ![Login Smoothwall](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/LoginSmooth.jpeg)
    
## Evidencias Windows XP

1. Direccionamiento IP

    ![Ip XP](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/dirXp.jpeg)
    
    ![Dirección IP XP](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/IpXp.jpeg)
    
2. Configuraciones realizadas en SmoothWall

    ![Configuración 1](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/conf1.jpeg)
    
    ![Configuración 2](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/conf2.jpeg)
    
3. Aplicaciones Instaladas

    ![Aplicaciones Instaladas](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/inst1.jpeg)
        
    ![Aplicaciones Instaladas](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/inst2.jpeg)
    
4. Instalación de VirtualBox Guest Additions

    ![Guest Drag](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/DragXp.png)
    
    ![Guest Drop](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/DropXp.png)
    
## Evidencia de Metasploitable

1. Instalación

    ![Instalación](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/Metasploitable.PNG)
    
2. Conectividad entre metasploitable

    - a) Con Windows XP
    
    ![Meta XP](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/metaXp.jpeg)
    
    - b) Con Kali Linux
    
    ![Meta Kali](https://gitlab.com/danielmunozo129/practica1-ciberseguridad/-/raw/master/Practica1Images/MetaKali.jpeg)
    